## Metal Archives Upcoming Releases RSS

This is a fork from https://gitlab.com/vincenttunru/feeds

My sole contribution is to configure VincentTunru's code to parse https://www.metal-archives.com/release/upcoming and periodically generate an RSS feed using Gitlab pipelines. Attempts update every 4 hours.

The feed can be found here: https://sonofevil.gitlab.io/feeds/releases.xml

You'll need some RSS reader to subscribe. I use FeedBro for Firefox.

Original Readme below:

# CI setup for Feed me up, Scotty!

You can fork this repository and edit `feeds.toml` to start generating your own
RSS feeds.

# Automatically generate feeds

## On GitHub

To automatically generate feeds twice a day on GitHub:

1. Fork this repository and edit `feeds.toml`.
2. Create a branch called `gh-pages`.
3. Visit the _Actions_ tab for your fork, then enable workflows for your fork.
4. Make sure the Publishing Source for your GitHub Pages site is set to the
   `gh-pages` branch.
   [Instructions here.](https://docs.github.com/en/pages/getting-started-with-github-pages/configuring-a-publishing-source-for-your-github-pages-site)

GitHub Actions will then automatically run twice a day to check for updates,
and publish your feeds at `https://<username>.github.io/feeds/`.

## On GitLab

To automatically generate feeds on GitLab:

1. Fork this repository and edit `feeds.toml`.
2. [Create a new pipeline schedule](./-/pipeline_schedules/new) (CI/CD ˃
   Schedules).
3. Set it to your desired interval (e.g. `30 5,17 * * *` to run at 5:30 and
   17:30).
4. Hit "Save pipeline schedule".

GitLab CI/CD will then automatically run at your selected interval to check for
updates, and publish your feeds at `https://<username>.gitlab.io/feeds/`.
